#ifndef _CHAR_ANIMA__CHARACTER_
#define _CHAR_ANIMA__CHARACTER_

#include "../Utils.hpp"

class Joint {
public:
  Joint(
    glm::vec3 localPosition0,
    glm::mat4 localOrientation0,
    Joint * parent = nullptr
  ):
  parent(parent),
  localOrientation0(localOrientation0), localOrientation(localOrientation0),
  localPosition0(localPosition0), localPosition(localPosition0) {}

  inline glm::mat4 getLocalRot() const {return localOrientation;}
  inline glm::vec3 getLocalTran() const {return localPosition;}
  inline Joint * getParent() {return parent;}

private:
  // current orientation in parent space
  glm::mat4 localOrientation = glm::mat4(0);
  // current position in parent space
  glm::vec3 localPosition = {0,0,0};

  // Initial orientation in parent space
  glm::mat4 localOrientation0 = glm::mat4(0);
  // Initial position in parent space
  glm::vec3 localPosition0 = {0,0,0};

  Joint * parent = nullptr;

public:
  /*Return the global orientation*/
  inline glm::mat4 getGlobalOrientation() const {
    if(parent) return parent->getGlobalOrientation() * localOrientation;
    else return localOrientation;
  }
  /*Return the global position*/
  inline glm::vec3 getGlobalPosition() const {
    if(parent) return parent->getGlobalPosition() + glm::vec3(parent->getGlobalOrientation()*glm::vec4(localPosition,1));
    else return localPosition;
  }

  /*Return the global orientation*/
  inline glm::mat4 getOriginalGlobalOrientation() const {
    if(parent) return parent->getOriginalGlobalOrientation() * localOrientation0;
    else return localOrientation0;
  }
  /*Return the global position*/
  inline glm::vec3 getOriginalGlobalPosition() const {
    if(parent) return parent->getOriginalGlobalPosition() + glm::vec3(parent->getOriginalGlobalOrientation()*glm::vec4(localPosition0,1));
    else return localPosition0;
  }

  /*Convert vector to the local space of the joint.*/
  inline glm::vec3 globalToLocal(const glm::vec3 & globalVec) {
    return glm::inverse(getGlobalOrientation())*glm::vec4(globalVec-getGlobalPosition(),1);
  }
  inline glm::vec3 localToGlobal(const glm::vec3 & localVec) {
    return glm::vec3(getGlobalOrientation()*glm::vec4(localVec,1))+getGlobalPosition();
  }

  inline void updateOriginal() {
    this->localOrientation0 = localOrientation;
    this->localPosition0 = localPosition;
  }

  inline void moveAnimate(const glm::vec3 & newGlobalPos) {
    if(parent) {
      localPosition = parent->globalToLocal(newGlobalPos);
    }
    else {
      localPosition = newGlobalPos;
    }
  }

  inline void rotateAnimate(const glm::mat4 & newLocalOrientation) {
    localOrientation = newLocalOrientation;
  }
};

struct Bone {
  Joint * a = nullptr;
  Joint * b = nullptr;
};

class Character {
public:
  Character() {}

  inline std::vector<Bone> & getBones() {return bones;}
  inline std::vector<Joint*> & getJoints() {return joints;}

  inline std::vector<glm::vec3> getJointsPositions() {
    std::vector<glm::vec3> positions;
    positions.reserve(joints.size());
    for(auto joint : joints) {
      positions.push_back(joint->getGlobalPosition());
    }
    return positions;
  }

  inline Joint * addJoint(const glm::vec3& pos, Joint * parent=nullptr) {
    if(parent) {
      glm::vec3 localPos = parent->globalToLocal(pos);
      glm::mat4 localOrientation = glm::mat4(1);
      Joint * joint = new Joint(localPos, localOrientation, parent);
      joints.push_back(joint);
      return joint;
    }
    else {
      glm::mat4 localOrientation = glm::mat4(1);
      Joint * joint = new Joint(pos, localOrientation, nullptr);
      joints.push_back(joint);
      return joint;
    }
  }

private:
  std::vector<std::vector<float>> skinWeights;
  std::vector<glm::vec3> originalVertices;

  std::vector<Joint*> joints;

  std::vector<Bone> bones;

public:
  inline void updateBones() {
    bones.clear();
    for(int j=0; j<joints.size(); j++) {
      Joint * joint = joints.at(j);
      if(joint->getParent()) {
        Bone b = {joint, joint->getParent()};
        bones.push_back(b);
      }
    }
  }

  enum Skinning {
    RIGID, SMOOTH
  };

  inline void generateSkining(
    const std::vector<glm::vec3> & originalVertices,
    Skinning skinning = Skinning::SMOOTH
  ) {
    skinWeights.clear();
    skinWeights.resize(originalVertices.size(), std::vector<float>());
    if(joints.size()==0) return;
    for(int v=0; v<originalVertices.size(); v++) {
      // For each vertex
      const glm::vec3 & vertex = originalVertices.at(v);
      // Set all joints weights to 0
      skinWeights.at(v).resize(joints.size(), 0.f);

      if(skinning==Skinning::RIGID) {
        // Find the closest joint
        int indexClosestJoint = 0;
        float distanceClosestJoint = FLT_MAX;
        for(int j=0; j<joints.size(); j++) {
          glm::vec3 jPos = joints[j]->getGlobalPosition();
          float distance = glm::length(jPos-vertex);
          if(distanceClosestJoint>distance) {
            indexClosestJoint = j;
            distanceClosestJoint = distance;
          }
        }
        // Set the weight of the closest joint to 1
        skinWeights.at(v)[indexClosestJoint] = 1.f;
      }
      else if(skinning==Skinning::SMOOTH) {
        float maxDistance = -FLT_MAX;
        float minDistance = FLT_MAX;
        for(int j=0; j<joints.size(); j++) {
          glm::vec3 jPos = joints[j]->getGlobalPosition();
          float distance = glm::length(jPos-vertex);
          skinWeights.at(v)[j] = distance;
          if(distance>maxDistance) {
            maxDistance = distance;
          }
          if(distance<minDistance) {
            minDistance = distance;
          }
        }
        for(float & w : skinWeights.at(v)) {
          float x = (w-minDistance)/(maxDistance-minDistance);
          w = std::exp(-x*x/0.05f);
        }
      }
    }
    this->originalVertices = originalVertices;

    for(Joint * joint : joints) {
      joint->updateOriginal();
    }
  }

  inline void updateMesh(
    std::vector<glm::vec3> & updatedVertices
  ) {
    if(originalVertices.size()!=skinWeights.size()) return;
    if(updatedVertices.size()!=originalVertices.size()) return;
    if(skinWeights.size()==0) return;
    if(skinWeights[0].size()!=joints.size()) return;

    for(int v=0; v<originalVertices.size(); v++) {
      const glm::vec3 & original = originalVertices[v];
      glm::vec3 update = {0,0,0};
      float weightSum = 0;
      for(int j=0; j<joints.size(); j++) {
        auto r0 = joints[j]->getOriginalGlobalOrientation();
        auto t0 = joints[j]->getOriginalGlobalPosition();
        auto r = joints[j]->getGlobalOrientation();
        auto t = joints[j]->getGlobalPosition();
        
        auto T = t-t0;
        auto R = glm::toMat4(glm::quat(r)-glm::quat(r0));
        
        update += skinWeights[v][j] * glm::vec3(R*glm::vec4(original+T,1));
        weightSum += skinWeights[v][j];
      }
      updatedVertices[v] = update*(1.0f/weightSum);
    }
  }
};

#endif
