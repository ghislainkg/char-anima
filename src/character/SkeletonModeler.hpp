#ifndef _CHAR_ANIMA__SKELETON_MODELER_
#define _CHAR_ANIMA__SKELETON_MODELER_

#include "../Utils.hpp"

#include "Character.hpp"

#include <GumRender3D.hpp>

class SkeletonModeler {
public:
  SkeletonModeler() {}
  SkeletonModeler(Character * character): character(character) {}

  inline void selectJoint(int index) {
    selectedJoint = character->getJoints().at(index);
    triggerViewUpdate();
  }
  inline void unselect() {
    selectedJoint = nullptr;
    triggerViewUpdate();
  }
  inline bool isSelectedJoint(Joint * joint) const {
    return joint == selectedJoint;
  }

  std::function<void()> updateView;

private:
  Joint * selectedJoint = nullptr;

  Character * character = nullptr;

  inline void triggerViewUpdate() {
    if(updateView) updateView();
  }

public:

  inline void addChildToSelectedJoint(
    const glm::vec2 & screenPos, const gr3d::Camera & camera
  ) {
    if(selectedJoint) {
      glm::vec3 pos = gr3d::MousePicking::pointScreenToWorld(
        screenPos, camera,
        glm::length(camera.position-selectedJoint->getGlobalPosition())
      );
      Joint * joint = character->addJoint(
        pos,
        selectedJoint
      );
      selectedJoint = joint;
      character->updateBones();
      triggerViewUpdate();
    }
  }

  inline void moveSelectedJoint(
    const glm::vec2 & screenPos, const gr3d::Camera & camera
  ) {
    if(selectedJoint) {
      glm::vec3 pos = gr3d::MousePicking::pointScreenToWorld(
        screenPos, camera,
        glm::length(camera.position-selectedJoint->getGlobalPosition())
      );
      selectedJoint->moveAnimate(pos);
      triggerViewUpdate();
    }
  }
};

#endif
