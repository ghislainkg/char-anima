#ifndef _CHAR_ANIMA__UTILS_
#define _CHAR_ANIMA__UTILS_

// GLM (for maths)
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/matrix_decompose.hpp>

// C++ STD library
#include <cstdlib>
#include <cstdio>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <string>
#include <cmath>
#include <memory>
#include <algorithm>
#include <functional>
#include <type_traits>

#define M_PI 3.14159265358979323846

#endif
