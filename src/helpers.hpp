#ifndef _CHAR_ANIMA__HELPERS_
#define _CHAR_ANIMA__HELPERS_

#include <GumRender3D.hpp>

#include "rigiddynamics/RigidSolver.hpp"

struct Body {
  RigidBody * body;
  gr3d::Mesh * bodyMesh;
  gr3d::PhongMaterial * bodyMatMesh;
  gr3d::MeshRenderingParams * bodyRenderingParams;
};

inline Body getBodyCylinder(
  const std::string & name,
  float mass,
  const glm::vec3 & pos,
  const glm::vec3 angles,
  float bodyRadius, float bodyLength,
  int details,
  const glm::vec3 & color, float alpha
) {
  Body body;
  body.bodyMesh = new gr3d::Mesh();
  body.bodyMatMesh = new gr3d::PhongMaterial();
  body.bodyRenderingParams = new gr3d::MeshRenderingParams();

  body.bodyMesh->init();
  gr3d::Shapes::createUnitCylinder(details, body.bodyMesh->getVertices(), body.bodyMesh->getTriangles());
  for(auto & v : body.bodyMesh->getVertices()) 
    v = glm::scale(glm::mat4(1),{bodyRadius*0.5f,bodyRadius*0.5f,bodyLength*0.5f})*glm::vec4(v,1.0);
  
  for(auto & v : body.bodyMesh->getVertices())
    body.bodyMesh->getNormals().push_back(glm::normalize(v));

  body.body = new CylinderRigidBody(
    mass, bodyRadius, bodyLength,
    body.bodyMesh->getVertices(), body.bodyMesh->getTriangles(), body.bodyMesh->getNormals()
  );
  body.body->name = name;

  body.body->x = pos;
  body.body->r = glm::quat(angles);
  
  body.bodyMesh->loadVertices();
  body.bodyMesh->loadTriangles();
  body.bodyMesh->loadNormals();
  body.bodyMesh->loadTexCoords();
  body.bodyMesh->loadColors();
  body.bodyMesh->pos = body.body->x;
  body.bodyMesh->angles = glm::eulerAngles(body.body->r);
  body.bodyMatMesh->ambientColor.initFromValues({u_char(255*color.r),u_char(255*color.g),u_char(255*color.b)},1,1,3);
  body.bodyMatMesh->diffuseColorMap.initFromValues({u_char(255*color.r),u_char(255*color.g),u_char(255*color.b)},1,1,3);
  body.bodyMatMesh->specularColorMap.initFromValues({200,200,200},1,1,3);
  body.bodyMatMesh->shininessMap.initFromValues({100},1,1,1);
  body.bodyMatMesh->normalMap.initFromValues({0,0,0},1,1,3);
  body.bodyMatMesh->alphaMap.initFromValues({u_char(255*alpha)},1,1,1);
  body.bodyRenderingParams->edgeColor = gr3d::DefaultColors::BLACK4;
  body.bodyRenderingParams->edgeThickness = 0.004f;
  body.bodyRenderingParams->normals = gr3d::MeshRenderingNormals::VERTEX_NORMALS;
  body.bodyRenderingParams->renderEdges = false;

  return body;
}

inline Body getBodyBox(
  const std::string & name,
  float mass,
  const glm::vec3 & pos,
  const glm::vec3 angles,
  const glm::vec3 dims,
  const glm::vec3 & color, float alpha
) {
  Body body;
  body.bodyMesh = new gr3d::Mesh();
  body.bodyMatMesh = new gr3d::PhongMaterial();
  body.bodyRenderingParams = new gr3d::MeshRenderingParams();

  body.bodyMesh->init();
  gr3d::Shapes::createUnitCube(body.bodyMesh->getVertices(), body.bodyMesh->getTriangles());
  for(auto & v : body.bodyMesh->getVertices())
    v = glm::scale(glm::mat4(1),dims*0.5f)*glm::vec4(v,1.0);

  for(auto & v : body.bodyMesh->getVertices())
    body.bodyMesh->getNormals().push_back(glm::normalize(v));

  body.body = new BoxRigidBody(
    mass, dims,
    body.bodyMesh->getVertices(), body.bodyMesh->getTriangles(), body.bodyMesh->getNormals()
  );
  body.body->name = name;

  body.body->x = pos;
  body.body->r = glm::quat(angles);
  
  body.bodyMesh->loadVertices();
  body.bodyMesh->loadTriangles();
  body.bodyMesh->loadNormals();
  body.bodyMesh->loadTexCoords();
  body.bodyMesh->loadColors();
  body.bodyMesh->pos = body.body->x;
  body.bodyMesh->angles = glm::eulerAngles(body.body->r);
  body.bodyMatMesh->ambientColor.initFromValues({u_char(255*color.r),u_char(255*color.g),u_char(255*color.b)},1,1,3);
  body.bodyMatMesh->diffuseColorMap.initFromValues({u_char(255*color.r),u_char(255*color.g),u_char(255*color.b)},1,1,3);
  body.bodyMatMesh->specularColorMap.initFromValues({200,200,200},1,1,3);
  body.bodyMatMesh->shininessMap.initFromValues({100},1,1,1);
  body.bodyMatMesh->normalMap.initFromValues({0,0,0},1,1,3);
  body.bodyMatMesh->alphaMap.initFromValues({u_char(255*alpha)},1,1,1);
  body.bodyRenderingParams->edgeColor = gr3d::DefaultColors::BLACK4;
  body.bodyRenderingParams->edgeThickness = 0.004f;
  body.bodyRenderingParams->normals = gr3d::MeshRenderingNormals::VERTEX_NORMALS;
  body.bodyRenderingParams->renderEdges = false;

  return body;
}

inline Body getBodySphere(
  const std::string & name,
  float mass,
  const glm::vec3 & pos,
  const glm::vec3 angles,
  float radius,
  int details,
  const glm::vec3 & color, float alpha
) {
  Body body;
  body.bodyMesh = new gr3d::Mesh();
  body.bodyMatMesh = new gr3d::PhongMaterial();
  body.bodyRenderingParams = new gr3d::MeshRenderingParams();

  body.bodyMesh->init();
  gr3d::Shapes::createUnitSphere(details, body.bodyMesh->getVertices(), body.bodyMesh->getTriangles());
  for(auto & v : body.bodyMesh->getVertices()) 
    v = glm::scale(glm::mat4(1),{radius*0.5f,radius*0.5f,radius*0.5f})*glm::vec4(v,1.0);
  
  for(auto & v : body.bodyMesh->getVertices())
    body.bodyMesh->getNormals().push_back(glm::normalize(v));

  body.body = new SphereRigidBody(
    mass, radius,
    body.bodyMesh->getVertices(), body.bodyMesh->getTriangles(), body.bodyMesh->getNormals()
  );
  body.body->name = name;

  body.body->x = pos;
  body.body->r = glm::quat(angles);
  
  body.bodyMesh->loadVertices();
  body.bodyMesh->loadTriangles();
  body.bodyMesh->loadNormals();
  body.bodyMesh->loadTexCoords();
  body.bodyMesh->loadColors();
  body.bodyMesh->pos = body.body->x;
  body.bodyMesh->angles = glm::eulerAngles(body.body->r);
  body.bodyMatMesh->ambientColor.initFromValues({u_char(255*color.r),u_char(255*color.g),u_char(255*color.b)},1,1,3);
  body.bodyMatMesh->diffuseColorMap.initFromValues({u_char(255*color.r),u_char(255*color.g),u_char(255*color.b)},1,1,3);
  body.bodyMatMesh->specularColorMap.initFromValues({200,200,200},1,1,3);
  body.bodyMatMesh->shininessMap.initFromValues({100},1,1,1);
  body.bodyMatMesh->normalMap.initFromValues({0,0,0},1,1,3);
  body.bodyMatMesh->alphaMap.initFromValues({u_char(255*alpha)},1,1,1);
  body.bodyRenderingParams->edgeColor = gr3d::DefaultColors::BLACK4;
  body.bodyRenderingParams->edgeThickness = 0.004f;
  body.bodyRenderingParams->normals = gr3d::MeshRenderingNormals::VERTEX_NORMALS;
  body.bodyRenderingParams->renderEdges = false;

  return body;
}

#endif
