#include <iostream>

#include <GumRender3D.hpp>

#include "character/Character.hpp"
#include "character/SkeletonModeler.hpp"

#include "modeling/Modeling.hpp"

#include "rigiddynamics/RigidSolver.hpp"

#include "helpers.hpp"

class AppWindow : public gr3d::Window3D {
public:
  AppWindow(): Window3D(800, 800, "Anim") {}

  gr3d::Renderer renderer;
  gr3d::MouseFocusController controller;
  gr3d::Mesh plane;
  gr3d::PhongMaterial matPlane;


// Mesh
  gr3d::Mesh mesh;
  gr3d::PhongMaterial matMesh;
  gr3d::MeshRenderingParams renderingParams;

  Modeler modeler;

// Skeleton
  std::vector<gr3d::Polyline> bonesPolylines;
  Character character;

  SkeletonModeler skeletonModeler;

// Rigid body
  RigidSolver solver;
  std::vector<Body> bodies;

protected:

  inline void afterInit() override {
    renderer.init();
    renderer.camera.position = {0, 6, 2.5};
    renderer.camera.target = {0, 0, 0};
    renderer.camera.type = gr3d::CameraType::PERSPECTIVE;
    renderer.camera.fov = 60.f;
    renderer.camera.near = 0.01f;
    renderer.camera.far = 1000.f;

    renderer.light.intensity = 1500;
    renderer.light.ambient = 0.001f;
    renderer.light.type = gr3d::LightType::POINT;

    controller = gr3d::MouseFocusController(&renderer.camera);
    controller.speed = 0.3f;

    mesh.init();
    gr3d::Shapes::createUnitSphere(40,mesh.getVertices(),mesh.getTriangles());
    mesh.loadVertices();
    mesh.loadTriangles();
    // Normals
    auto normals = mesh.getVertices();
    for(auto & n : normals) n = glm::normalize(n);
    mesh.updateNormals(std::move(normals));
    // Colors
    auto colors = mesh.getVertices();
    for(auto & c : colors) c = {0, 0, 0};
    mesh.updateColors(colors);
    // texture coordinates
    std::vector<glm::vec2> texCoords;
    texCoords.resize(mesh.getVertices().size());
    for(auto & t : texCoords) t = {0, 0};
    mesh.updateTexCoords(texCoords);

    modeler = Modeler(&mesh);

    plane.init();
    gr3d::Shapes::createUnitPlane(plane.getVertices(), plane.getTriangles());
    for(auto & v : plane.getVertices()) v = glm::eulerAngleX(M_PI/2)*glm::vec4(v,1.0);
    for(auto & v : plane.getVertices()) v = glm::scale(glm::mat4(1),{4,4,4})*glm::vec4(v,1.0);
    plane.loadVertices();
    plane.loadTriangles();
    plane.recomputeNormals();
    plane.loadNormals();
    plane.loadTexCoords();
    plane.loadColors();
    plane.pos = {0,-1.5,0};

    matPlane.ambientColor.initFromValues({200,200,5},1,1,3);
    matPlane.diffuseColorMap.initFromValues({200,200,5},1,1,3);
    matPlane.specularColorMap.initFromValues({200,200,200},1,1,3);
    matPlane.shininessMap.initFromValues({100},1,1,1);
    matPlane.normalMap.initFromValues({0,0,0},1,1,3);
    matPlane.alphaMap.initFromValues({200},1,1,1);

    matMesh.ambientColor.initFromValues({200,5,200},1,1,3);
    matMesh.diffuseColorMap.initFromValues({200,5,200},1,1,3);
    matMesh.specularColorMap.initFromValues({200,200,200},1,1,3);
    matMesh.shininessMap.initFromValues({100},1,1,1);
    matMesh.normalMap.initFromValues({0,0,0},1,1,3);
    matMesh.alphaMap.initFromValues({200},1,1,1);

    renderingParams.edgeColor = gr3d::DefaultColors::WHITE4;
    renderingParams.vertexColor = gr3d::DefaultColors::RED4;
    renderingParams.edgeThickness = 0.003f;
    renderingParams.vertexThickness = 0.01f;
    renderingParams.normals = gr3d::MeshRenderingNormals::VERTEX_NORMALS;
    renderingParams.renderEdges = true;
    renderingParams.renderVertices = false;

    auto joint0 = character.addJoint({0.0f, 0.0f, 0.0f});
    character.addJoint({0.0f, 0.5f, 0.0f}, joint0);
    character.updateBones();
    skeletonModeler = SkeletonModeler(&character);
    skeletonModeler.updateView = [&]() {
      updateSkeletonDisplay();
    };
    updateSkeletonDisplay();


    Body b;
    // b = getBodyCylinder("cylinder", 0.0005f, {-0.75,4,0}, {1,0,0}, 1.0f, 1.5f, 10, gr3d::DefaultColors::ORANGE3, 1);
    // b.body->restit = 0.000000001f;
    // bodies.push_back(b);
    // solver.bodies.push_back(b.body);

    // b = getBodySphere("sphere", 0.0005f, {1.75,3,0}, {0,1,0}, 2.0f, 10, gr3d::DefaultColors::YELLOW3, 1);
    // b.body->restit = 0.000000001f;
    // bodies.push_back(b);
    // solver.bodies.push_back(b.body);

    b = getBodyBox("box-blue", 1000, {-0.75,-1,0}, {0,0.1,0.1}, {1.2,1.2,1.2}, gr3d::DefaultColors::BLUE3, 1);
    b.body->restit = 0.99f;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);

    b = getBodyBox("box-red", 1000, {-0.75,1,0}, {0,0.3,0.1}, {2.5,2.5,2.5}, gr3d::DefaultColors::RED3, 1);
    b.body->restit = 0.99f;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);

    b = getBodyBox("box-yellow", 1000, {1.75,-1,1}, {0,0.1,0.1}, {1.5,1.5,1.5}, gr3d::DefaultColors::YELLOW3, 1);
    b.body->restit = 0.99f;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);

    b = getBodyBox("box-green", 1000, {1.75,0,-1}, {M_PI/4,0.1,0.1}, {1.5,1.5,1.5}, gr3d::DefaultColors::GREEN3, 1);
    b.body->restit = 0.99f;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);

    b = getBodyBox("box-black", 1000, {1.75,3,-1}, {0,0,0}, {1.5,3.5,1.5}, gr3d::DefaultColors::BLACK3, 1);
    b.body->restit = 0.99f;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);

    // b = getBodyBox("box-white", 1000, {0,6,0}, {0,0,0}, {3.5,1.5,3.5}, gr3d::DefaultColors::WHITE3, 1);
    // b.body->restit = 0.99f;
    // bodies.push_back(b);
    // solver.bodies.push_back(b.body);

    float alpha = 0.1f;
    //
    b = getBodyBox("plane-bottom", 100, {0,-5,0}, {0,0,0}, {50,3,50}, gr3d::DefaultColors::GREEN3, alpha);
    b.body->restit = 1.0f;
    b.body->simulate = false;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);
    //
    b = getBodyBox("plane-top", 100, {0,12,0}, {0,0,0}, {50,3,50}, gr3d::DefaultColors::GREEN3, alpha);
    b.body->restit = 1.0f;
    b.body->simulate = false;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);
    //
    b = getBodyBox("wall-left", 100, {10,0,0}, {0,0,0}, {1,25,20}, gr3d::DefaultColors::PURPLE3, alpha);
    b.body->restit = 1.0f;
    b.body->simulate = false;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);
    //
    b = getBodyBox("wall-right", 100, {-10,0,0}, {0,0,0}, {1,25,20}, gr3d::DefaultColors::PURPLE3, alpha);
    b.body->restit = 1.0f;
    b.body->simulate = false;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);
    //
    b = getBodyBox("wall-front", 100, {0,0,-10}, {0,0,0}, {20,25,1}, gr3d::DefaultColors::PURPLE3, alpha);
    b.body->restit = 1.0f;
    b.body->simulate = false;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);
    //
    b = getBodyBox("wall-back", 100, {0,0,10}, {0,0,0}, {20,25,1}, gr3d::DefaultColors::PURPLE3, alpha);
    b.body->restit = 1.0f;
    b.body->simulate = false;
    bodies.push_back(b);
    solver.bodies.push_back(b.body);
  }

  bool play = false;

  float time = 0;
  float stepTime = 0.01f;

  void render(float deltaTime) override {
    // In case of viewport update
    renderer.camera.width = width;
    renderer.camera.height = height;
    // Clear frame
    glClearColor(0.2, 0.2, 0.2, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glDisable(GL_CULL_FACE);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Render mesh
    // glEnable(GL_DEPTH_TEST);
    // renderer.renderMeshLighting(&mesh, &matMesh, &renderingParams);
    // renderer.renderMeshLighting(&plane, &matPlane, &renderingParams);

    // // Render bones
    // glDisable(GL_DEPTH_TEST);
    // for(auto & polyline : bonesPolylines) {
    //   renderer.renderPolyline(&polyline);
    //   renderer.renderPolylinePoints(&polyline);
    // }

    // render body
    glEnable(GL_DEPTH_TEST);
    // glDisable(GL_BLEND);

    if(play) {
      time += deltaTime;
      if(time >= stepTime) {
        solver.step(stepTime);
        time -= stepTime;
      }
      // play = false;
    }

    for(Body & b : bodies) {
      b.bodyMesh->pos = b.body->x;
      b.bodyMesh->angles = glm::eulerAngles(b.body->r);
      renderer.renderMeshLighting(b.bodyMesh, b.bodyMatMesh, b.bodyRenderingParams);
    }

    // renderer.camera.target = bodies[0].body->x;
  };

  void beforeEnd() override {
    mesh.destroy();
  }

  void onKeyPush(gr3d::KeyCode key, gr3d::KeyDevice device) override {
    if(key==gr3d::KeyCode::S) {
      // Select/Unselect a joint
      int jointIndex = gr3d::MousePicking::vertexPicking(
        character.getJointsPositions(), glm::mat4(1),
        renderer.camera,
        controller.getMouseScreenPos(), 0.1f
      );
      if(jointIndex>=0) {
        skeletonModeler.selectJoint(jointIndex);
      }
      else {
        skeletonModeler.unselect();
      }
    }
    else if(key==gr3d::KeyCode::A) {
      // Add new joint
      skeletonModeler.addChildToSelectedJoint(
        controller.getMouseScreenPos(), renderer.camera
      );
    }
    else if(key==gr3d::KeyCode::G) {
      //  Generate skinning weights
      character.generateSkining(mesh.getVertices());
    }
    else if(key==gr3d::KeyCode::W) {
      gr3d::Geometry::meshSubdivision(mesh.getVertices(), mesh.getTriangles());
      mesh.recomputeNormals();
      mesh.loadVertices();
      mesh.loadTriangles();
    }
    else if(key==gr3d::KeyCode::Z) {
      solver.simulate = !solver.simulate;
    }
    else if(key==gr3d::KeyCode::P) {
      play = true;
    }
  }

  void onKeyDown(gr3d::KeyCode key, gr3d::KeyDevice device) override {
    if(key==gr3d::KeyCode::MOUSE_LEFT) {
      controller.onMousePress();
    }
    else if(key==gr3d::KeyCode::D) {
      // Move selected joint
      skeletonModeler.moveSelectedJoint(
        controller.getMouseScreenPos(), renderer.camera
      );
      character.updateMesh(mesh.getVertices());
      mesh.loadVertices();
    }
    else if(key==gr3d::KeyCode::E) {
      // Apply brush
      glm::vec3 center;
      if(shouldBrushMesh(center)) {
        modeler.applyBumpBrush(center);
      }
    }
    else if(key==gr3d::KeyCode::Q) {
      // Apply brush
      if(!modeler.getIsDisplaceBrushing()) {
        glm::vec3 center;
        if(shouldBrushMesh(center)) {
          modeler.startDisplaceBrush(center);
        }
      }
      else {
        glm::vec3 worldPos = gr3d::MousePicking::pointScreenToWorld(
          controller.getMouseScreenPos(), renderer.camera,
          glm::length(renderer.camera.position-modeler.getCurrentDisplaceBrushCenter())
        );
        modeler.applyDisplaceBrush(worldPos);
      }
    }
  };

  void onKeyUp(gr3d::KeyCode key, gr3d::KeyDevice device) override {
    if(key==gr3d::KeyCode::Q) {
      modeler.stopDisplaceBrush();
    }
  }

  void onMouseMove(float x, float y) override {
    controller.onMouseMove(x,y);
  };
  void onMouseScroll(float y) override {
    controller.onMouseScroll(y);
  };

  // Update the display of the skeleton
  void updateSkeletonDisplay() {
    auto & bones = character.getBones();
    bonesPolylines.clear();
    bonesPolylines.resize(bones.size(), gr3d::Polyline());
    for(int b=0; b<bones.size(); b++) {
      auto & bone = bones.at(b);
      gr3d::Polyline & line = bonesPolylines.at(b);
      line.init();
      line.updateVertices({
        bone.a->getGlobalPosition(),
        bone.b->getGlobalPosition()
      });
      line.updateColors({
        skeletonModeler.isSelectedJoint(bone.a)?gr3d::DefaultColors::WHITE3:gr3d::DefaultColors::BLUE3,
        skeletonModeler.isSelectedJoint(bone.b)?gr3d::DefaultColors::WHITE3:gr3d::DefaultColors::BLUE3,
      });
      line.lineColor = gr3d::DefaultColors::GREEN4;
      line.lineColor.a = 0.6f;
    }
  }

  bool shouldBrushMesh(glm::vec3 & center) {
    glm::vec3 rayStart, rayDirection;
    gr3d::Geometry::makeCameraRay(
      renderer.camera,controller.getMouseScreenPos(),
      rayStart, rayDirection
    );
    return gr3d::Geometry::rayMeshIntersection(
      rayStart, rayDirection,
      mesh.getVertices(), mesh.getTriangles(),
      center
    );
  }
};

int main() {
  AppWindow app;
  app.init();
  app.loop();
  app.end();
  return 0;
}

