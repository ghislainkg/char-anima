#ifndef _CHAR_ANIMA__MODELING_
#define _CHAR_ANIMA__MODELING_

#include "../Utils.hpp"

class Modeler {
public:
  Modeler() {}
  Modeler(gr3d::Mesh * mesh): mesh(mesh) {}

  float brushStrengh = 0.1f;
  float brushRadius = 0.5f;

private:
  gr3d::Mesh * mesh = nullptr;

  glm::vec3 currentDisplaceBrushCenter = {0,0,0};
  glm::vec3 lastDisplaceBrushPos = {0,0,0};
  bool isDisplaceBrushing = false;

public:

// Bump brush
  inline void applyBumpBrush(
    const glm::vec3 & center
  ) {
    std::vector<glm::vec3> & vertices = mesh->getVertices();
    const std::vector<glm::vec3> & normals = mesh->getNormals();
    for(int i=0; i<vertices.size(); i++) {
      glm::vec3 & v = vertices[i];
      float d = glm::length(v-center);
      if(d<=brushRadius) {
        float x = d/brushRadius;
        float amplitude = brushStrengh*std::exp(-x*x/0.1f);
        glm::vec3 displace = glm::normalize(normals[i])*amplitude;
        if(displace.x!=displace.x) {
          assert(false);
        }
        v += displace;
      }
    }
    mesh->recomputeNormals();
    mesh->loadVertices();
  }

// Displace brush

  inline void startDisplaceBrush(
    const glm::vec3 & center
  ) {
    if(!isDisplaceBrushing) {
      currentDisplaceBrushCenter = center;
      lastDisplaceBrushPos = center;
      isDisplaceBrushing = true;
    }
  }
  inline bool getIsDisplaceBrushing() const {return isDisplaceBrushing;}
  inline glm::vec3 getCurrentDisplaceBrushCenter() const {return currentDisplaceBrushCenter;}
  inline void applyDisplaceBrush(
    const glm::vec3 & pos
  ) {
    if(isDisplaceBrushing && glm::length(pos-lastDisplaceBrushPos)>0) {
      std::vector<glm::vec3> & vertices = mesh->getVertices();
      const std::vector<glm::vec3> & normals = mesh->getNormals();
      for(int i=0; i<vertices.size(); i++) {
        glm::vec3 & v = vertices[i];
        float d = glm::length(v-currentDisplaceBrushCenter);
        if(d<=brushRadius) {
          float x = d/brushRadius;
          float amplitude = brushStrengh*std::exp(-x*x/0.1f);
          glm::vec3 displace = glm::normalize(pos-lastDisplaceBrushPos)*amplitude;
          v += displace;
        }
      }
      lastDisplaceBrushPos = pos;
      mesh->recomputeNormals();
      mesh->loadVertices();
    }
  }
  inline void stopDisplaceBrush() {
    isDisplaceBrushing = false;
    currentDisplaceBrushCenter = {0,0,0};
    lastDisplaceBrushPos = {0,0,0};
  }
};

#endif
