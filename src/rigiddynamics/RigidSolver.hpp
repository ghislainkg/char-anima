#ifndef _CHAR_ANIMA__RIGID_SOLVER_
#define _CHAR_ANIMA__RIGID_SOLVER_

#include "../Utils.hpp"

#include "RigidBody.hpp"

class RigidSolver {
public:
  RigidSolver() {}

  inline void init() {
    stepi = 0;
    time = 0;
  }

  inline void step(float dt) {
    if(!simulate) return;
    this->dt = dt;

    resetForcesAndTorques();
    addWeightForcesAndTorques();
    updateMomentumsFromForceTorque(dt);

    savePrevStates();
    updatePosOrientation(dt);
    addCollisionResponseMomentums();
    resumePrevStates();

    // addWeightForcesAndTorques();
    // updateMomentumsFromForceTorque(dt);

    updatePosOrientation(dt);

    stepi += 1;
    time += dt;
  }

  std::vector<RigidBody*> bodies;
  glm::vec3 g = {0, -9.8f, 0};

  bool simulate = false;

  int stepi = 0; // Current simulation step index
  float time = 0; // Current simulation time
  float dt = 0;
private:

  inline void savePrevStates() {
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      body->prevx = body->x;
      body->prevr = body->r;
    }
  }
  inline void resumePrevStates() {
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      body->x = body->prevx;
      body->r = body->prevr;
    }
  }

  inline void updateMomentumsFromForceTorque(float dt) {
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      if(body->simulate) {
        // Update momentums
        body->P = body->P + dt*body->F; // linear momentum
        body->L = body->L + dt*body->tau; // angular momentum
      }
    }
  }
  inline void updatePosOrientation(float dt) {
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      if(body->simulate) {
        // Update position and orientation
        body->v = body->linearVelocity();
        body->w = body->angularVelocity();

        body->x = body->x + dt*body->v; // position
        body->r = 
          glm::toMat3(body->r) + 
          dt*glm::matrixCross3(body->w)*glm::toMat3(body->r); // orientation  

        body->r = glm::normalize(body->r);      
      }
    }
  }

  inline void resetForcesAndTorques() {
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      body->F = {0,0,0};
      body->tau = {0,0,0};
    }
  }
  inline std::vector<RigidBody*> getOtherBodies(int index) const {
    std::vector<RigidBody*> others;
    others.reserve(bodies.size()-1);
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      others.push_back(body);
    }
    return others;
  }
  inline void handleCollision(
    RigidBody * body1, RigidBody * body2,
    const std::vector<glm::vec3> & pointsGlobal,
    const std::vector<glm::vec3> & normals_1to2
  ) {
    for(int k=0; k<pointsGlobal.size(); k++) {
      glm::vec3 pointGlobal = pointsGlobal[k];
      glm::vec3 normal_1to2 = normals_1to2[k];
      normal_1to2 = glm::normalize(normal_1to2);
      // linear and angular velocities
      const glm::vec3 & v1 = body1->v;
      const glm::vec3 & v2 = body2->v;
      const glm::vec3 & w1 = body1->w;
      const glm::vec3 & w2 = body2->w;
      // Collision point in local spaces
      glm::vec3 r1 = pointGlobal-body1->x;
      glm::vec3 r2 = pointGlobal-body2->x;
      // Collision point velocity in local spaces
      glm::vec3 vi1 = glm::cross(w1,r1) + v1;
      glm::vec3 vi2 = glm::cross(w2,r2) + v2;
      // Relative velocity
      glm::vec3 vrel = vi2-vi1;
      // Restitution coefficient
      float e = 0.5f*(body1->restit+body2->restit);
      // Impulse magnetude
      float impulse = 
        -((1+e)*glm::dot(vrel,normal_1to2))/
        (
          (body1->simulate?1/body1->mass:0) +
          (body2->simulate?1/body2->mass:0) +
          glm::dot(
            (
              glm::cross(body1->inertiaInverse()*(glm::cross(r1,normal_1to2)), r1) +
              glm::cross(body2->inertiaInverse()*(glm::cross(r2,normal_1to2)), r2)
            ),
            normal_1to2
          )
        );
      std::cout 
        << impulse << "   " 
        << glm::dot(vrel,normal_1to2) << "   " 
        << normal_1to2 << "   " 
        << vrel << "   " 
        << glm::dot(vrel,normal_1to2)
        << std::endl;
      float threshold = 0.001f;
      if(
        threshold >=
        glm::dot(vrel,normal_1to2) && glm::dot(vrel,normal_1to2)
        >= -threshold
      ) {
        // Resting contact
        glm::vec3 f = -dt*body1->F;
        body1->P += f;
        body2->P += f;
        body1->L += glm::cross(r1,f);
        body2->L += glm::cross(r2,f);
        std::cout << "RESTING" << std::endl;
      }
      else if(
        glm::dot(vrel,normal_1to2) > threshold
      ) {
        // Update linear momentum
        body1->P -= impulse*normal_1to2;
        body2->P += impulse*normal_1to2;
        // Update angular momentum
        body1->L -= impulse*glm::cross(r1,normal_1to2);
        body2->L += impulse*glm::cross(r2,normal_1to2);
      }
    }
  }
  inline void addCollisionResponseMomentums() {
    for(int i=0; i<bodies.size(); i++) {
      RigidBody * body1 = bodies[i];
      for(int j=i+1; j<bodies.size(); j++) {
        RigidBody * body2 = bodies[j];
        if(body1->simulate || body2->simulate) {
          std::vector<glm::vec3> pointsGlobal;
          std::vector<glm::vec3> normals_1to2;
          if(body1->collide(body2, pointsGlobal, normals_1to2)) {
            std::cout << "collision " << i <<" -> "<< j << std::endl;
            std::cout << "\t " << body1->name << "  " << body2->name << std::endl;
            handleCollision(body1,body2,pointsGlobal,normals_1to2);
          }
          else if(body2->collide(body1, pointsGlobal, normals_1to2)) {
            std::cout << "collision " << i <<" -> "<< j << std::endl;
            std::cout << "\t " << body2->name << "  " << body1->name << std::endl;
            handleCollision(body2,body1,pointsGlobal,normals_1to2);
          }
        }
      }
    }
  }
  inline void addWeightForcesAndTorques() {
    for(int b=0; b<bodies.size(); b++) {
      RigidBody * body = bodies[b];
      body->F += g*body->mass;
      for(auto & v : body->vertices) {
        body->tau += glm::cross(body->toGlobalSpace(v)-body->x, body->F);
      }
    }
  }
};

#endif
