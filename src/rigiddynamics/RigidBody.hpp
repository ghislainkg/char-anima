#ifndef _CHAR_ANIMA__RIGID_BODY_
#define _CHAR_ANIMA__RIGID_BODY_

#include "../Utils.hpp"

#include <GumRender3D.hpp>

#include <queue>

enum RigidBodyType {
  CYLINDER, SPHERE, BOX
};

class RigidBody {
    friend class RigidSolver;
public:
  RigidBody() {}
  RigidBody(float mass): mass(mass) {}

  virtual RigidBodyType getType() const = 0;
  virtual bool isInside(const glm::vec3 & p) const = 0;

  glm::mat4 getWorldMat() const {
    return glm::translate(x)*glm::toMat4(r);
  }

  inline glm::mat3 inertiaInverse() const {
    return glm::toMat3(r)*I0inv*glm::transpose(glm::toMat3(r));
  }
  inline glm::vec3 linearVelocity() const {
    return (1.f/mass)* P;
  }
  inline glm::vec3 angularVelocity() const {
    return inertiaInverse()*L;
  }

  glm::vec3 x = {0,0,0}; // position
  glm::quat r = glm::quat(glm::vec3(0,0,0)); // orientation

  // For collision detection
  glm::vec3 prevx = {0,0,0};
  glm::quat prevr = glm::quat(glm::vec3(0,0,0));

  bool simulate = true;

  std::string name = "";
  float restit = 0.4f;

  inline bool collide(
    RigidBody * other,
    std::vector<glm::vec3> & pointsGlobal, std::vector<glm::vec3> & normals
  ) const {
    pointsGlobal.clear();
    normals.clear();
    for(int i=0; i<vertices.size(); i++) {
      glm::vec3 v = vertices[i];
      glm::vec3 vGlobal = this->toGlobalSpace(v);
      glm::vec3 vGlobalprev = this->toGlobalSpacePrev(v);
      if(other->isInside(vGlobal) && !other->isInside(vGlobalprev)) {
        std::cout << "Inside - Outside" << std::endl;
        bool foundTriangle = false;
        for(int j=0; j<other->triangles.size(); j++) {
          glm::uvec3 t = other->triangles[j];
          glm::vec3 point;
          if(
            (
              gr3d::Geometry::rayTriangleIntersection(
                vGlobalprev, glm::normalize(vGlobal-vGlobalprev),
                other->toGlobalSpace(other->vertices[t[0]]),
                other->toGlobalSpace(other->vertices[t[1]]),
                other->toGlobalSpace(other->vertices[t[2]]),
                point
              )
              ||
              gr3d::Geometry::rayTriangleIntersection(
                vGlobalprev, glm::normalize(vGlobalprev-vGlobal),
                other->toGlobalSpace(other->vertices[t[0]]),
                other->toGlobalSpace(other->vertices[t[1]]),
                other->toGlobalSpace(other->vertices[t[2]]),
                point
              )
            )
            &&
            glm::length(this->toGlobalSpacePrev(v)-vGlobal)
              >=
            glm::length(this->toGlobalSpacePrev(v)-point)
          ) {
            glm::vec3 n = other->getTriangleNormal(j);
            pointsGlobal.push_back(point);
            normals.push_back(n);
            std::cout << "nnnnnnnnnnnnnnnnnnn:   " << n << std::endl;
            // normals.push_back({0,1,0});
            foundTriangle = true;
            break;
          }
        }
      }
      else if(other->isInside(vGlobal) && other->isInside(vGlobalprev)) {
        std::cout << "Inside - Inside" << std::endl;
        bool foundTriangle = false;
        float minDist = FLT_MAX;
        glm::vec3 foundNormal;
        glm::vec3 foundPoint;
        for(int j=0; j<other->triangles.size(); j++) {
          glm::uvec3 t = other->triangles[j];
          glm::vec3 point;
          if(
            gr3d::Geometry::rayTriangleIntersection(
              vGlobalprev, glm::normalize(vGlobalprev-vGlobal),
              other->toGlobalSpace(other->vertices[t[0]]),
              other->toGlobalSpace(other->vertices[t[1]]),
              other->toGlobalSpace(other->vertices[t[2]]),
              point
            )
          ) {
            foundTriangle = true;
            float dist = glm::length(point-vGlobalprev);
            if(minDist>dist) {
              minDist = dist;
              foundNormal = other->getTriangleNormal(j);
              foundPoint = point;
            }
          }
        }
        if(foundTriangle) {
          pointsGlobal.push_back(vGlobal);
          normals.push_back(foundNormal);
          std::cout << "nnnnnnnnnnnnnnnnnnn:   " << foundNormal << std::endl;
        }
      }
    }
    return !pointsGlobal.empty();
  }

protected:
  // Vertices for collisions
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::uvec3> triangles;

  static inline glm::vec3 average(const std::vector<glm::vec3>& vs) {
    glm::vec3 ave = {0,0,0};
    for(const glm::vec3 & v : vs) {
      ave += v;
    }
    return (1.f/vs.size())*ave;
  }
  inline glm::vec3 getTriangleNormal(int tIndex) {
    glm::uvec3 t = triangles[tIndex];
    glm::vec3 n = glm::normalize(glm::cross(
      toGlobalSpace(vertices[t[1]])-toGlobalSpace(vertices[t[0]]),
      toGlobalSpace(vertices[t[2]])-toGlobalSpace(vertices[t[0]])
    ));
    glm::vec3 n_ave = glm::normalize(average({
      glm::normalize(normals[t[0]]),
      glm::normalize(normals[t[1]]),
      glm::normalize(normals[t[2]])
    }));
    if(glm::dot(n_ave,n)<0) n = -n;
    return n;
  }

  inline std::vector<int> closestVertices(
    const glm::vec3 & p, int count
  ) {
    struct Cmp{
      bool operator()(const std::pair<int,float>& a, const std::pair<int,float>& b){
        return a.second > b.second;
      }
    };
    std::priority_queue<
      std::pair<int,float>, std::vector<std::pair<int,float>>, Cmp
    > queue;
    for(int i=0; i<vertices.size(); i++) {
      auto v = toGlobalSpace(vertices[i]);
      float d = glm::length(v-p);
      queue.push({i,d});
    }
    std::vector<int> closests;
    for(int i=0; i<count; i++) {
      auto f = queue.top();
      queue.pop();
      closests.push_back(f.first);
    }
    return closests;
  }

  float mass; // mass
  glm::mat3 I0; // inertia tensor in body space
  glm::mat3 I0inv; // inverse inertia tensor in body space

  glm::vec3 F = {0,0,0}; // Force
  glm::vec3 tau = {0,0,0}; // Torque

  glm::vec3 P = {0,0,0}; // linear momentum
  glm::vec3 L = {0,0,0}; // angular momentum

  glm::vec3 v = {0,0,0}; // linear velocity
  glm::vec3 w = {0,0,0}; // angular velocity
  
  inline glm::vec3 toLocalSpace(const glm::vec3 & p) const {
    return glm::inverse(glm::toMat3(r))*(p - x);
  }
  inline glm::vec3 toGlobalSpace(const glm::vec3 & p) const {
    return glm::toMat3(r)*p + x;
  }

  inline glm::vec3 toLocalSpacePrev(const glm::vec3 & p) const {
    return glm::inverse(glm::toMat3(prevr))*(p - prevx);
  }
  inline glm::vec3 toGlobalSpacePrev(const glm::vec3 & p) const {
    return glm::toMat3(prevr)*p + prevx;
  }
};

class CylinderRigidBody: public RigidBody {
public:
  CylinderRigidBody() {}
  CylinderRigidBody(
    float mass, float radius, float lengthZ,
    const std::vector<glm::vec3> & vertices,
    const std::vector<glm::uvec3> & triangles,
    const std::vector<glm::vec3> & normals
  ):
    RigidBody(mass), radius(radius), lengthZ(lengthZ)
  {
    this->vertices = vertices;
    this->triangles = triangles;
    this->normals = normals;
    I0 = glm::diagonal3x3(glm::vec3(
      (1.f/12)*mass*(3*radius*radius+lengthZ*lengthZ), // x
      (1.f/12)*mass*(3*radius*radius+lengthZ*lengthZ), // y
      (1.f/12)*mass*radius*radius // z
    ));
    I0inv = glm::diagonal3x3(glm::vec3(
      1.f/((1.f/12)*mass*(3*radius*radius+lengthZ*lengthZ)), // x
      1.f/((1.f/12)*mass*(3*radius*radius+lengthZ*lengthZ)), // y
      1.f/((1.f/12)*mass*radius*radius) // z
    ));
  }

  RigidBodyType getType() const override {return RigidBodyType::CYLINDER;}

  float lengthZ;
  float radius;

  inline bool isInside(const glm::vec3 & p) const override {
    glm::vec3 pLocal = toLocalSpace(p);
    if(
      glm::length(glm::vec2(pLocal.x,pLocal.y)) <= radius &&
      std::abs(pLocal.z) <= lengthZ*0.5f
    ) {
      return true;
    }
    return false;
  }
};

class SphereRigidBody: public RigidBody {
public:
  SphereRigidBody() {}
  SphereRigidBody(
    float mass, float radius,
    const std::vector<glm::vec3> & vertices,
    const std::vector<glm::uvec3> & triangles,
    const std::vector<glm::vec3> & normals
  ):
    RigidBody(mass), radius(radius)
  {
    this->vertices = vertices;
    this->triangles = triangles;
    this->normals = normals;
    I0 = glm::diagonal3x3(glm::vec3(
      (2.f/5)*mass*radius, // x
      (2.f/5)*mass*radius, // y
      (2.f/5)*mass*radius // z
    ));
    I0inv = glm::diagonal3x3(glm::vec3(
      1.f/((2.f/5)*mass*radius), // x
      1.f/((2.f/5)*mass*radius), // y
      1.f/((2.f/5)*mass*radius) // z
    ));
  }

  RigidBodyType getType() const override {return RigidBodyType::SPHERE;}

  float radius;

  inline bool isInside(const glm::vec3 & p) const override {
    glm::vec3 pLocal = toLocalSpace(p);
    if(glm::length(pLocal) <= radius) {
      return true;
    }
    return false;
  }
};

class BoxRigidBody: public RigidBody {
public:
  BoxRigidBody() {}
  BoxRigidBody(
    float mass, const glm::vec3 & dims,
    const std::vector<glm::vec3> & vertices,
    const std::vector<glm::uvec3> & triangles,
    const std::vector<glm::vec3> & normals
  ):
    RigidBody(mass), dims(dims)
  {
    this->vertices = vertices;
    this->triangles = triangles;
    this->normals = normals;
    I0 = glm::diagonal3x3(glm::vec3(
      (1.f/12)*mass*(dims.y*dims.y + dims.z*dims.z), // x
      (1.f/12)*mass*(dims.x*dims.x + dims.z*dims.z), // y
      (1.f/12)*mass*(dims.x*dims.x + dims.y*dims.y) // z
    ));
    I0inv = glm::diagonal3x3(glm::vec3(
      1.f/((1.f/12)*mass*(dims.y*dims.y + dims.z*dims.z)), // x
      1.f/((1.f/12)*mass*(dims.x*dims.x + dims.z*dims.z)), // y
      1.f/((1.f/12)*mass*(dims.x*dims.x + dims.y*dims.y)) // z
    ));
  }

  RigidBodyType getType() const override {return RigidBodyType::BOX;}

  glm::vec3 dims;

  inline bool isInside(const glm::vec3 & p) const override {
    glm::vec3 pLocal = toLocalSpace(p);
    if(
      std::abs(pLocal.x) <= dims.x*0.5f &&
      std::abs(pLocal.y) <= dims.y*0.5f &&
      std::abs(pLocal.z) <= dims.z*0.5f
    ) {
      return true;
    }
    return false;
  }
};

#endif
